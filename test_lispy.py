import unittest

from lispy import main, InvalidLispProgramException


class LispyTestCase(unittest.TestCase):
    def test_missing_outer_list(self):
        with self.assertRaises(InvalidLispProgramException) as cm:
            main('invalid program')
        self.assertEqual(str(cm.exception), 'The program does not contain an outer list.')

    def test_addition(self):
        self.assertEqual(main('(+ 2 3)'), 5.0)

    def test_subtraction(self):
        self.assertEqual(main('(- 2 5)'), -3.0)

    def test_multiplication(self):
        self.assertEqual(main('(* 10 50)'), 500.0)

    def test_division(self):
        self.assertEqual(main('(/ 2 5)'), 0.4)

    def test_nested_operations(self):
        self.assertEqual(main('(/ (+ 2 10) 3)'), 4)

    def test_equal_operation(self):
        self.assertEqual(main('(eq? 1 1)'), True)

    def test_equal_operation_false(self):
        self.assertEqual(main('(eq? 1 2)'), False)

    def test_quote(self):
        self.assertEqual(main('(quote a)'), '\'a')

    def test_quote_list(self):
        self.assertEqual(main('(quote \'(1 2 3))'), '\'(1 2 3)')

    def test_atom(self):
        self.assertTrue(main('(atom? 3)'))

    def test_atom_false(self):
        self.assertFalse(main('(atom? \'(1 2 3))'))

if __name__ == '__main__':
    unittest.main()
