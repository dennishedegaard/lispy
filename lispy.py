class InvalidLispProgramException(Exception):
    """
    This `Exception` is thrown if a program being run is not valid LISP code.

    The `Exception` may provide a `message` describing
    """
    pass


def _parse_list(string):
    """
    Takes a list as a string, and parses it into a list of elements, where the elements may be lists themselves.

    :param string: The string to parse into a list.
    :type string: str.
    :returns: A parsed list of the string.
    :rtype: list.
    """
    result = []
    current_item = ''
    depth = 0
    in_symbol = None

    # If the string starts with a list, remove it.
    if string.startswith('('):
        string = string[1:]
    if string.endswith(')'):
        string = string[:-1]

    # Iterate chars and build the result
    for char in string:
        if char == ' ' and depth == 0:
            if current_item:
                result.append(current_item)
                current_item = ''
        elif char == '(' and in_symbol is None:
            depth += 1
            if in_symbol is not None:
                in_symbol = depth
        elif char == ')' and in_symbol is None:
            if in_symbol == depth:
                in_symbol = None
            depth -= 1
            result.append(_parse_list(current_item))
            current_item = ''
        elif char == '\'':
            in_symbol = depth
        else:
            current_item += char
    if current_item:
        result.append(current_item)
    return result


def _evaluate(item):
    if isinstance(item, (list, tuple)):
        return _evaluate_list(item)
    else:
        try:
            return float(item)
        except ValueError:
            pass

        try:
            return int(item)
        except ValueError:
            return str(item)


def _evaluate_list(l):
    """
    :param l: The list to evaluate.
    :type l: list.
    :return: The result of the evaluation, if any result.
    """
    operator = l[0]
    if operator == '+':
        return _evaluate(l[1]) + _evaluate(l[2])
    elif operator == '-':
        return _evaluate(l[1]) - _evaluate(l[2])
    elif operator == '*':
        return _evaluate(l[1]) * _evaluate(l[2])
    elif operator == '/':
        return _evaluate(l[1]) / _evaluate(l[2])
    elif operator == 'atom?':
        return not isinstance(_evaluate(l[1]), (tuple, list))
    elif operator == 'eq?':
        return _evaluate(l[1]) == _evaluate(l[2])
    elif operator == 'quote':
        return '\'' + ' '.join(l[1:])
    elif len(l) == 1:
        return _evaluate(l[0])
    else:
        raise NotImplementedError('Unknown operator: %s' % operator)


def main(program):
    """
    Takes a program as text, evaluates it and returns the result.

    :param program: The program as a string.
    :type program: str.
    :returns: The of the ended program, as whatever type it returns.
    """
    if not program.startswith('(') and not program.endswith(')'):
        raise InvalidLispProgramException('The program does not contain an outer list.')
    return _evaluate_list(_parse_list(program))

if __name__ == '__main__':
    print main('(/ (+ 2 10) 3)')